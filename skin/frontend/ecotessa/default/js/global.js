/* global document, $, $$, Meanbee, window */

(function() {
    'use strict';

    document.observe("dom:loaded", function() {

        //vars
        var dropdownMenu,
            minicart,
            slick;

        //fn

        //callback

        if(Virtua.DropdownMenu){
            var dropdownMenu= new Virtua.DropdownMenu();
        }

        if(Virtua.Minicart){
            var minicart= new Virtua.Minicart();
        }

        if(Virtua.Slick){
            var slick= new Virtua.Slick();
        }

        if(Virtua.Fadein){
            var slick= new Virtua.Fadein();
        }

    });

})();
