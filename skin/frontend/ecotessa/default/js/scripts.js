/**
 * Represents decorating an accordian menu list with classes for css use.
 * @constructor
 * @param {string} options - Object of class names.
 */

(function () {
    'use strict';

    /* global window, Class, Event, event, document, $$ */

    if (!window.Virtua) {
        window.Virtua = {};
    }

    //Dropdown menu

    var DropdownMenu = Class.create({

        initialize: function (options) {

            this.options = Object.extend({}, options || {});


            //init functions

            this.use();
        },

        use: function () {
            jQuery('.dropdown-menu > a').click(function (e) {
                e.preventDefault();

                jQuery('.dropdown-menu > ul').toggle('hide');
            });

        }

    });

    window.Virtua.DropdownMenu = DropdownMenu;

    //Minicart

    var Minicart = Class.create({

        initialize: function (options) {

            this.options = Object.extend({}, options || {});


            //init functions

            this.use();
        },

        use: function () {
            jQuery(document).on('click', '.skip-link', function (e) {
                e.preventDefault();

                jQuery(this).toggleClass('skip-active');
                jQuery('.block.block-cart.skip-content').toggleClass('skip-active');
            });

            jQuery(document).on('click', '.skip-link-close', function (e) {
                e.preventDefault();

                jQuery(this).toggleClass('skip-active');
                jQuery('.block.block-cart.skip-content').toggleClass('skip-active');
            });


            jQuery('.nav-icon1').click(function () {
                jQuery(this).toggleClass('open');
                jQuery('.main-nav').toggle('hide');
            });

        }

    });

    window.Virtua.Minicart = Minicart;

    //Slick

    var Slick = Class.create({

        initialize: function (options) {

            this.options = Object.extend({}, options || {});


            //init functions

            this.use();
        },

        use: function () {
            jQuery('.home-slider-best-seller').slick({
                dots: false,
                infinite: false,
                speed: 300,
                prevArrow: "<i class='fa fa-3x fa-angle-left slick-prev slick-arrow'></i>",
                nextArrow: "<i class='fa fa-3x fa-angle-right slick-next slick-arrow'></i>",
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            jQuery('select').wrap('<div class="select" />');

            jQuery('.show-discount > h2').click(function () {
                jQuery('.accordin-shipping-body-l >li > h2').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.shipping').hide(100, function () {
                    jQuery('.discount').show(100);
                });

            });

            jQuery('.show-tax > h2').click(function () {
                jQuery('.accordin-shipping-body-l >li > h2').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.discount').hide(100, function () {
                    jQuery('.shipping').show(100);
                });

            });

            jQuery(document).on('keyup change', '#ccsave_cc_owner', function() {
                var dInput = jQuery( this ).val();
                jQuery('.card-holder > div').html(dInput);
            });

            jQuery(document).on('keyup change', '#ccsave_cc_number', function() {
                var dInput = jQuery( this ).val();
                jQuery('.numberr').html(dInput);
            });

            jQuery(document).on('change', '#ccsave_expiration, #ccsave_expiration_yr', function() {
                var m = jQuery('#ccsave_expiration').val();
                var m = (m < 10) ? '0' + m : m;
                var y = jQuery('#ccsave_expiration_yr').val().substr(2,2);
                jQuery('.card-expiration-date > div').html(m + '/' + y);
            });

            jQuery(document).on('change', '#ccsave_cc_type', function() {

                var card = jQuery('#ccsave_cc_type').val();
                if (card == 'AE') {
                    jQuery('.credit-card .logo > img').hide();
                    jQuery('.american-express').show(100);
                }else if (card == 'VI') {
                    jQuery('.credit-card .logo > img').hide();
                    jQuery('.visa').show(100);
                }else if (card == 'MC') {
                    jQuery('.credit-card .logo > img').hide();
                    jQuery('.master-card').show(100);
                }else if (card == 'DI') {
                    jQuery('.credit-card .logo > img').hide();
                    jQuery('.discover').show(100);
                }
            });

            jQuery(document).on('focus', '#ccsave_cc_cid', function() {
                jQuery('.credit-card-box').addClass('hover');
            }).on('blur', '#ccsave_cc_cid', function(){
                jQuery('.credit-card-box').removeClass('hover');
            }).on('keyup change', '#ccsave_cc_cid', function(){
                var dInput = jQuery( this ).val();
                jQuery('.ccv > div').html(dInput);
            });
        }

    });

    window.Virtua.Slick = Slick;

    //Fadein

    var Fadein = Class.create({

        initialize: function (options) {

            this.options = Object.extend({}, options || {});


            //init functions

            this.use();
        },

        use: function () {
            jQuery(window).scroll(function () {

                /* Check the location of each desired element */
                jQuery('.hideme-fadein').each(function (i) {

                    var bottom_of_object = jQuery(this).offset().top + jQuery(this).outerHeight();
                    var bottom_of_window = jQuery(window).scrollTop() + jQuery(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if (bottom_of_window > bottom_of_object) {

                        jQuery(this).addClass('fadein-after');

                    }

                });

            });

        }

    });

    window.Virtua.Fadein = Fadein;


})();
