/**
 * Represents decorating an accordian menu list with classes for css use.
 * @constructor
 * @param {string} options - Object of class names.
 */

(function () {
    'use strict';

    /* global window, Class, Event, event, document, $$ */

    if (!window.Virtua) {
        window.Virtua = {};
    }

    var TestClass = Class.create({

        initialize: function( options ) {

            this.options = Object.extend({
                testOption: 'testValue'
            }, options || {} );


            //init functions

            this.testFunction();
        },

        testFunction: function() {
            alert('działa');
        }

    });

    window.Virtua.TestClass = TestClass;

})();